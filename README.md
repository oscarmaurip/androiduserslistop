# androidUsersListOp

Retrieve data rest - MVVM kotlin

Android Code Challenge
Based on AndroidStudio - kotlin - MVVM, PR2S Project
By: Oscar Pastás


# Technical test for Android

This test app consists of a list of users, and data. Allow to filter data by name

. This test should be design with latest version of Xcode and for latest version of Android.
1. The app should have these basic screens:
    - [Main screen](#main-screen) (list of users)
    - [User screen](#user-screen - data personal info)
1. The code should follow MVVM
1. A good use of REST services (for fetching data) is encouraged.

## Main screen

The first screen to show is a table of users, fetched from the `/users` resource of [JSONPlaceholder](https://jsonplaceholder.typicode.com).

![Main screen](screens/Main@1x.png)

## User screen

This screen shows the details of an user resource of [JSONPlaceholder](https://jsonplaceholder.typicode.com)).

![User screen](screens/User@1x.png)


