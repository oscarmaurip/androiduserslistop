package com.example.android.networkconnect.datafragment.model

import com.google.gson.annotations.SerializedName

// Class to retrieve response from retrofit call to host
data class DataResponse(
    @SerializedName("name")
    val name: String?,
    @SerializedName("email")
    val species: String?,
    @SerializedName("phone")
    val image: String?
)