package com.example.android.networkconnect.datafragment.model

import com.google.gson.annotations.SerializedName

data class Character(
    @SerializedName("name")
    val name: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("phone")
    val phone: String?,
    @SerializedName("website")
    val website: String?,
    @SerializedName("company")
    val company: Company?,
    @SerializedName("address")
    val address: Address?
)